import React, {useState, useEffect} from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import DailyWeather from '../app/containers/DailyWeather';
import SevenDailyWeather from '../app/containers/SevenDailyWeather';


const AppNavigator = () => {
  const Stack = createNativeStackNavigator();

  return (
    <Stack.Navigator initialRouteName={'DailyWeather'}>
      <Stack.Screen name="DailyWeather" component={DailyWeather} />
      <Stack.Screen name="SevenDailyWeather" component={SevenDailyWeather} />
    </Stack.Navigator>
  );
};
export default AppNavigator;
