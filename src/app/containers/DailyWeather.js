import React, { useEffect, useState } from "react";
import { View, LogBox } from "react-native";
LogBox.ignoreLogs(['Warning: ...']);
LogBox.ignoreAllLogs();
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import {
  Button,
  Image,
  Box,
  AspectRatio,
  Center,
  Stack,
  HStack,
  VStack,
  Input,
  Text,
  Modal,
} from "native-base";
const axios = require("axios");

function DailyWeather({ navigation, route }) {
  const [inputText, setInputText] = useState("");
  const [weatherDay, setWeatherDay] = useState([]);
  const [coordenadas, setCoordenadas] = useState([]);
  const [nameCiudad, setNameCiudad] = useState("");
  const [searchTown, setSearchTown] = useState(false);
  const [dataSevenDays, setDataSevenDays] = useState([]);
  const [error, setError] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const paramsCiudad = { q: inputText };

  const searchLocation = async (queryParamas) => {
    const resp = await axios.get("https://search.reservamos.mx/api/v2/places", {
      params: queryParamas,
    });
    const dataCiudad = resp.data;
    const result = dataCiudad.map((item, index) => [item.lat, item.long]);
    setCoordenadas(result && result[0]);
    setNameCiudad(dataCiudad && dataCiudad[0].city_name);
  };

  useEffect(() => {
    if (inputText.length > 0) {
      searchLocation(paramsCiudad);
    }
  }, [inputText]);

  const searchWeatherDaily = async () => {
    try {
      const resp = await axios.get(
        `https://api.openweathermap.org/data/2.5/onecall?lat=${coordenadas[0]}&lon=${coordenadas[1]}&exclude=hourly&appid=a5a47c18197737e8eeca634cd6acb581`
      );
      const dataDaily = resp.data;
      const resultDaily = Object.values(dataDaily);
      const SevenDays = resultDaily[6];
      const filterSevenDays = SevenDays.map((item, index) => [
        item.temp.min,
        item.temp.max,
      ]);
      setWeatherDay(filterSevenDays[0]);
      setSearchTown(true);
      setDataSevenDays(SevenDays);
    } catch (error) {
      setShowModal(true);
      setError(true);
    }
  };

  const onChangeText = (text) => {
    setInputText(text);
    setWeatherDay([]);
    setSearchTown(false);
  };

  const renderInput = () => (
    <Center>
      <Stack mt={3} space={4} w="75%" maxW="300px">
        <Input
          size="2xl"
          onChangeText={onChangeText}
          variant="filled"
          placeholder="Buscar ciudad"
        />
      </Stack>
    </Center>
  );
  const renderButtonFooter = () => (
    <Center paddingTop={8}>
      <Button
        colorScheme="light"
        style={{
          marginBottom: 30,
          borderRadius: 20,
          paddingTop: 12,
          paddingBottom: 20,
          paddingRight: 60,
          paddingLeft: 60,
        }}
        onPress={() => searchWeatherDaily()}
      >
        Buscar
      </Button>
    </Center>
  );
  const renderCard = () => (
    <Box alignItems="center">
      <Box
        maxW="350"
        rounded="lg"
        overflow="hidden"
        marginBottom={2}
        borderColor="coolGray.200"
        borderWidth="1"
        _dark={{
          borderColor: "coolGray.600",
          backgroundColor: "gray.700",
        }}
        _web={{
          shadow: 2,
          borderWidth: 0,
        }}
        _light={{
          backgroundColor: "gray.50",
        }}
      >
        <Box>
          <AspectRatio w="100%" ratio={10 / 10}>
            <Image
              source={require("../../assets/sol.png")}
              alt="image"
              size={350}
            />
          </AspectRatio>
        </Box>
        <HStack>
          <Stack paddingTop={5} paddingLeft={15}>
            <Center>
              <MaterialCommunityIcons
                name={"thermometer"}
                color={"grey"}
                size={40}
              />
              <Text style={{ color: `#a9a9a9` }}>MAX</Text>
            </Center>
          </Stack>
          <Stack paddingTop={2}>
            <Text fontSize={35} style={{ color: `#a9a9a9` }}>
              {weatherDay.length && Math.round(weatherDay[0])} Fº
            </Text>
          </Stack>
          <Stack paddingLeft={25} paddingTop={5}>
            <Center>
              <MaterialCommunityIcons
                name={"thermometer"}
                color={"grey"}
                size={40}
              />
              <Text style={{ color: `#a9a9a9` }}>MIN</Text>
            </Center>
          </Stack>
          <Stack paddingTop={2}>
            <Text fontSize={35} style={{ color: `#a9a9a9` }}>
              {weatherDay.length && Math.round(weatherDay[1])} Fº
            </Text>
          </Stack>
        </HStack>

        <HStack>
          <Stack>
            <Text
              paddingLeft={4}
              paddingTop={2}
              fontSize={15}
              style={{ color: `#a9a9a9` }}
            >
              {" "}
              {searchTown && "Hoy"}{" "}
            </Text>
            <Text paddingLeft={5} fontSize={15} style={{ color: `#a9a9a9` }}>
              {searchTown && nameCiudad}{" "}
            </Text>
          </Stack>
          <Stack>
            {searchTown && (
              <Button
                colorScheme="light"
                style={{
                  marginBottom: 30,
                  borderRadius: 20,
                  paddingTop: 12,
                  paddingBottom: 12,
                  paddingRight: 10,
                  paddingLeft: 10,
                  marginLeft: 50,
                  marginTop: 10,
                }}
                onPress={() =>
                  navigation.navigate("SevenDailyWeather", { dataSevenDays })
                }
              >
                Siguientes 7 dias
              </Button>
            )}
          </Stack>
        </HStack>
      </Box>
    </Box>
  );

  const renderName = () => (
    <Center>
      <Text fontSize={30} style={{ color: `#a9a9a9` }}>
        {inputText.length > 0 && nameCiudad}{" "}
      </Text>
    </Center>
  );

  const renderModal = () => (
    <Modal
      isOpen={showModal}
      onClose={() => setShowModal(false)}
      _backdrop={{
        _dark: {
          bg: "coolGray.800",
        },
        bg: "warmGray.50",
      }}
    >
      <Modal.Content maxWidth="350" maxH="212">
        <Modal.CloseButton />
        <Modal.Header>Sin Resultados</Modal.Header>
        <Modal.Body>
          no encontramos resultados favorables, debes realizar una nueva
          busqueda
        </Modal.Body>
        <Modal.Footer>
          <Center>
            <Button
              colorScheme="light"
              style={{
                marginBottom: 30,
                borderRadius: 20,
                paddingTop: 12,
                paddingBottom: 12,
                paddingRight: 10,
                paddingLeft: 10,
                marginLeft: 50,
                marginTop: 10,
              }}
              onPress={() => setShowModal(false)}
            >
              Entendido
            </Button>
          </Center>
        </Modal.Footer>
      </Modal.Content>
    </Modal>
  );

  return (
    <View>
      <View alignItems={"flex-start"} marginTop={10} marginLeft={10}></View>

      {renderCard()}
      {renderName()}
      {renderInput()}
      {renderButtonFooter()}
      {renderModal()}
    </View>
  );
}

export default DailyWeather;
