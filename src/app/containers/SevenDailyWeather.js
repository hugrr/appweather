import React, {useEffect, useState} from 'react';
import {View} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  Button,
  Image,
  Box,
  AspectRatio,
  Center,
  Stack,
  HStack,
  VStack,
  Input,
  Text,
  FlatList,
} from 'native-base';

function SevenDailyWeather({navigation, route}) {
    const {dataSevenDays} = route.params;
    dataSevenDays.length ===8 && dataSevenDays.shift()

  const renderItem = ({item}) => (
    <Box alignItems="center">
      <Box
      
        maxW="350"
        rounded="lg"
        overflow="hidden"
        marginBottom={5}
        borderColor="coolGray.200"
        borderWidth="1"
        _dark={{
          borderColor: 'coolGray.600',
          backgroundColor: 'gray.700',
        }}
        _web={{
          shadow: 2,
          borderWidth: 0,
        }}
        _light={{
          backgroundColor: 'gray.50',
        }}>
        <Box>
          <AspectRatio  ratio={1 / 3}>
          <Image source={require('../../assets/sol.png')} alt="image" size={20}/>
          </AspectRatio>
        </Box>
        <HStack>
          <Stack paddingTop={5}marginLeft={95} >
            <Center>
              <MaterialCommunityIcons
                name={'thermometer'}
                color={'grey'}
                size={20}
              />
              <Text style={{color: `#a9a9a9`}}>MAX</Text>
            </Center>
          </Stack>
          <Stack paddingTop={1} >
          <Text fontSize={30} style={{color: `#a9a9a9`}}>{Math.round(item.temp.max)} Fº</Text>
          </Stack>
          <Stack marginLeft={18} paddingTop={5}  >
         
            <Center>
              <MaterialCommunityIcons
                name={'thermometer'}
                color={'grey'}
                size={20}
              />
              <Text style={{color: `#a9a9a9`}}>MIN</Text>
            </Center>
          </Stack>
          <Stack paddingTop={1} marginRight={4}>
          <Text fontSize={30} style={{color: `#a9a9a9`}}>{Math.round(item.temp.min)}Fº</Text>
          </Stack>  
        </HStack>
      </Box>
    </Box>
  );


  const renderFlatList=()=>(

    <FlatList
    data={dataSevenDays}
    renderItem={renderItem}
  />


  )
  
  
  return (
    <View>
      <View marginTop={40} marginLeft={10}>
       
      </View>
      {renderFlatList()}
    </View>
  );
}

export default SevenDailyWeather;
